package controller;


import java.sql.Connection;
import java.sql.DriverManager;

import java.util.Date;
import java.util.HashMap;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


public class JasperReportController {

    public static void runReport(String reportFile,String itemType, String toDate, String fromDate) {
        HashMap parameters=new HashMap();
        parameters.put("to_date", toDate);
        parameters.put("from_date",fromDate);
        parameters.put("item_type", itemType);
        
        
        try {
            JasperDesign jasperDesign = JRXmlLoader.load(reportFile);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            Connection jdbcConnection = connectDB(getDBUrl(), getDBUser(), getDBPassword());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jdbcConnection);
            JasperViewer.viewReport(jasperPrint, false);

        } catch (Exception ex) {
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            System.out.println(connectMsg);
        }
    }

    private static Connection connectDB(String databaseName, String userName, String password) {
        Connection jdbcConnection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            jdbcConnection = DriverManager.getConnection(databaseName, userName, password);
        } catch (Exception ex) {
            String connectMsg = "Could not connect to the database: " + ex.getMessage() + " " + ex.getLocalizedMessage();
            System.out.println(connectMsg);
        }
        return jdbcConnection;
    }

   

   

    private static String getDBUrl() {
        return "jdbc:oracle:thin:@localhost:1521/xe";
    }

    private static String getDBUser() {
        return "avik";
    }

    private static String getDBPassword() {
        return "avik";
    }

    
}
