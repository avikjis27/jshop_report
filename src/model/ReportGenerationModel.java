/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import util.DataBaseState;

/**
 *
 * @author IBM_ADMIN
 */
public class ReportGenerationModel extends Observable {
    
    private static ReportGenerationModel reportGenerationModel=null;
    
    /*Report part*/
    private List reportType;
    private List reportSubType;
    private List itemType;
    
    private boolean configuringReport;
    
    private Date toDate;
    private Date fromDate;
    
    /*Report Configuration*/
    
    private ReportGenerationModel()
    {
        setItemType();
        setReportSubType();
        setReportType(reportType);
    }
    
    public void registerObserver(Observer observer)
    {
        addObserver(observer);
    }
    
    public void deRegisterObserver(Observer observer)
    {
        deleteObserver(observer);
    }
    
    public static ReportGenerationModel getReportGenerationModel() 
    {
        if(reportGenerationModel==null)
        {
            reportGenerationModel= new ReportGenerationModel();
            return reportGenerationModel;
        }
        else
        {
            return reportGenerationModel;
        }
    }
    public static boolean destroyReportGenerationModel(ReportGenerationModel model)
    {
        if(model.equals(reportGenerationModel))
        {
            reportGenerationModel=null;
            return true;
        }
        else
        {
            System.out.println("Calling Destroymethod for different model object hence failed");
            return false;
        }
    }

    public List getReportType() {
        return reportType;
    }

    private void setReportType(List reportType) {
        this.reportType = reportType;
    }

    public List getReportSubType() {
        return reportSubType;
    }

    private void setReportSubType() {
        this.reportSubType = reportSubType;
    }

    public List getItemType() {
        return itemType;
    }

    private void setItemType() {
        itemType= DataBaseState.getAllItemType();
    }

    
    

   

    public boolean isConfiguringReport() {
        return configuringReport;
    }

    public void setConfiguringReport(boolean configuringReport) {
        this.configuringReport = configuringReport;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }
    
    
    
}
