/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DBEngine;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.swing.SwingWorker;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

import persistency.HibernateActivity;

/**
 *
 * @author IBM_ADMIN
 */


public class DatabaseEngine {
    
    public static ArrayList getUnsoldInventoryReport()
    {
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Query query=session.createSQLQuery("select * from item_details  where sold_v='N' order by stock_entry_date_d desc");
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        ArrayList list=(ArrayList)query.list();
        return list;
            
    }
    public static ArrayList getSoldItemsReport()
    {
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Query query=session.createSQLQuery("select a.*,b.sell_price_n,b.discount_n from item_details a,sold_item_details b where a.item_model_v=b.item_model_number_v and a.item_serial_number_v=b.item_id_v and  a.sold_v='Y' order by a.sold_on_date_d desc");
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        ArrayList list=(ArrayList)query.list();
        return list;
            
    }
    
    /*public static void main(String[] argc)
    {
        DatabaseEngine db=new DatabaseEngine();
        db.getUnsoldInventoryReport();
    }*/
    
}
