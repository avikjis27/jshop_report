package report;
//import frames.ReportPanel;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

public class InventoryReport extends Observable {

    public void generateInventoryReport() {
        
        Runtime runtime1 = Runtime.getRuntime();

        Process process;
        try {
            process = runtime1.exec("sqlplus avik/avik @unsoldInventory.sql "+new Date());

            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String output;
            while ((output = bufferedreader.readLine()) != null) {
                System.out.println(output);
            }
            process.destroy();
            //spoolFileRename("UNSOLD_INVENTORY.csv");
            



        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            setChanged();
            notifyObservers("Unsold Inventory Reprot Failed");
        }



    }
    
    public void soldInventoryReport() {
        
        Runtime runtime1 = Runtime.getRuntime();

        Process process;
        try {
            process = runtime1.exec("sqlplus avik/avik @soldInventory.sql "+new Date());

            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String output;
            while ((output = bufferedreader.readLine()) != null) {
                System.out.println(output);
            }
            process.destroy();
            //spoolFileRename("SOLD_INVENTORY.csv");
            



        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            setChanged();
            notifyObservers("Unsold Inventory Reprot Failed");
        }



    }

    public void spoolFileRename(String filename) {
        File spoolFile = new File(filename);
        String[] name=filename.split(".");
        Date date = new Date();
        String time_now = new SimpleDateFormat("ddMMMyyyyHHmm").format(date);
        filename = name[0] + "_" + time_now+".cvs";
        spoolFile.renameTo(new File(filename));
        
            

    }
}
